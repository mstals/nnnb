var myCanvas    = $("#myCanvas");
var c           = myCanvas.get(0);
var pointBag    = [];
var stage	= 1; //1 - attālumu noteikšana; 2 - tuvākais kaimiņš
var point_cnt	= 1;
var ctx = c.getContext("2d");
ctx.font = "20px Arial";
var personBag   = [
    {
        gender  : "vīrietis",
        height  : 182,          //cm
        weight  : 82,           //kg
        footsize: 30,            //cm
        prcheight:  0,          //prc - proporcionālās vērtības, kas tiks izrēķinātas vēlāk (0 līdz 1)
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "vīrietis",
        height  : 180,          
        weight  : 86,           
        footsize: 28,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "vīrietis",
        height  : 170,          
        weight  : 77,           
        footsize: 30,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0          
    },
    {
        gender  : "vīrietis",
        height  : 170,          
        weight  : 75,           
        footsize: 25,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "sieviete",
        height  : 152,          
        weight  : 45,           
        footsize: 15,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "sieviete",
        height  : 167,          
        weight  : 68,           
        footsize: 20,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "sieviete",
        height  : 165,          
        weight  : 59,           
        footsize: 18,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    },
    {
        gender  : "sieviete",
        height  : 175,          
        weight  : 68,           
        footsize: 23,
        prcheight:  0,        
        prcweitht:  0,
        prcfootsize:0
    }
];
function draw_axes(ctx)
{
    ctx.lineWidth   = 1;
    ctx.strokeStyle = "black";
    //y ass
    ctx.moveTo(20,480);
    ctx.lineTo(20,20);
    ctx.stroke();
    //y ass bultinas
    ctx.moveTo(10, 30);
    ctx.lineTo(20,20);
    ctx.stroke();
    //ctx.moveTo(20,20);
    ctx.lineTo(30,30);
    ctx.stroke();
    //y ass labelis
    ctx.fillText("y",5,45);

    // x ass
    ctx.moveTo(20,480);
    ctx.lineTo(480,480);
    ctx.stroke();
    //x ass bultinas
    ctx.moveTo(470,470);
    ctx.lineTo(480,480);
    ctx.stroke();    
    ctx.lineTo(470,490);
    ctx.stroke();
    //x ass labelis
    ctx.fillText("x",460,495);
}

draw_axes(ctx);
myCanvas.mousemove(function(event){
    $("#x_math_coords").text(event.pageX - myCanvas.offset().left - 20);
    $("#y_math_coords").text(parseFloat(480 - event.pageY - myCanvas.offset().top + 623).toFixed(0));
});
myCanvas.click({ctx: ctx, pointBag: pointBag}, function(event){
	var x_coords        = event.pageX - myCanvas.offset().left;
	var y_coords        = event.pageY - myCanvas.offset().top;
	var x_math_coords   = event.pageX - myCanvas.offset().left - 20;
	var y_math_coords   = parseFloat(480 - event.pageY - myCanvas.offset().top + 623).toFixed(0);
        var i;
        var tmpPoint;
        var nnCalcLog       = [];
        var nnCalcLogLine   = {};
        var nnPoint;
	if(stage == 1){
		var point_name  = "A";
		var point_color = "red";
		if(pointBag.length < 2)
		{
			ctx.beginPath();
			if(pointBag.length == 1){   //otrs punkts
				point_color     = "blue";            
				point_name      = "B";
			}else{        
				point_color     = "red";
			}        
			ctx.strokeStyle = point_color;
			ctx.lineWidth   = 3;
			ctx.arc(x_coords,y_coords,4,0,2*Math.PI);
			ctx.stroke();
			ctx.font = "12px Arial";
			ctx.fillText(point_name +" ("+x_math_coords+", "+y_math_coords+")",x_coords-30,y_coords+20);
			point = {
				name	: point_name,
				x   	: x_math_coords,
				y   	: y_math_coords,
				x_html	: x_coords,
				y_html	: y_coords,
				team	: point_color            
			};
			pointBag.push(point);			
		}
		if(pointBag.length==2){  //ir abi punkti        
			$("#euclidean_dist").text(parseFloat(euclidean_dist(pointBag[0], pointBag[1])).toFixed(2));
			$("#manhattan_dist").text(parseFloat(manhattan_dist(pointBag[0], pointBag[1])).toFixed(2));
			draw_euclidean_dist(ctx, pointBag[0], pointBag[1]);
			draw_manhattan_dist(ctx, pointBag[0], pointBag[1]);
		}
	}
        if(stage == 2)
        {
		ctx.beginPath();
		point_name	 = "P" + point_cnt;
		point = {
			name	: point_name,
			x   	: x_math_coords,
			y   	: y_math_coords,
			x_html	: x_coords,
			y_html	: y_coords,
			team	: point_color,
			nn_point: {}
		};
                for(i=0; i< pointBag.length; i++){
                    tmpPoint = pointBag[i];
                    //console.log(tmpPoint.name + euclidean_dist(point, tmpPoint));
                    if(i==0){
                        nnPoint = tmpPoint;
                    }
                    if(euclidean_dist(point, tmpPoint) < euclidean_dist(point, nnPoint)){
                        nnPoint = tmpPoint;
                    }
                    
                    nnCalcLogLine = {
                        name            : point.name + "<->" + tmpPoint.name,
                        euclidean_dist  : euclidean_dist(point, tmpPoint)                        
                    };
                    nnCalcLog.push(nnCalcLogLine);       
                }
                point.nn_point  = nnPoint;
                point.team      = nnPoint.team;                
		pointBag.push(point);
		point_cnt++;                
		ctx.strokeStyle = point.team//point_color;
		ctx.lineWidth   = 3;
		ctx.arc(x_coords,y_coords,4,0,2*Math.PI);
		ctx.stroke();
		ctx.font = "12px Arial";
		ctx.fillText(point_name,x_coords-7,y_coords+20);
	}	
        buildPointTable();
});
$("#reset").click(function(){
    clear_stage();
});
$("#calc").click(function(){
    classifyPerson({
        height: $("#person-height").val(),
        weight: $("#person-weight").val(),
        footsize: $("#person-foot-size").val(),
        prcheight:  0,
        prcweight:  0,
        prcfootsize:0
    });
});
function clear_stage(){
    $("#pointTable").empty();
    $("#bayesTable").empty();
    $("#nnTable").empty();
    ctx.beginPath();
    ctx.clearRect(0, 0, c.width, c.height);
	ctx.setLineDash([0,0]);
	ctx.strokeStyle = "black";
    pointBag = [];
	point_cnt	= 1    
    $("#euclidean_dist").text(0);
    $("#manhattan_dist").text(0);
    draw_axes(ctx);   
    switch(stage){
        case 2  :   generateSamplePoints(); break;
        case 3  :   buildPersonTable();     break;
    }        
}
function draw_euclidean_dist(ctx, A, B){
    ctx.strokeStyle = "black";    
    ctx.setLineDash([1,5]);	
    ctx.moveTo(A.x_html,A.y_html);
    ctx.lineTo(B.x_html,B.y_html);
    ctx.stroke();
}
function draw_manhattan_dist(ctx, A, B){
    ctx.strokeStyle = "black";
    ctx.setLineDash([1,5]);	
    ctx.moveTo(A.x_html,A.y_html);
    ctx.lineTo(B.x_html,A.y_html);
    ctx.stroke();
    ctx.moveTo(B.x_html,A.y_html);
    ctx.lineTo(B.x_html,B.y_html);
    ctx.stroke();
}
function euclidean_dist(A, B){    
    return Math.sqrt(Math.pow(A.x - B.x, 2) + Math.pow(A.y - B.y, 2));
}
function euclidean_dist_three_dimensional(A, B){    
    return Math.sqrt(Math.pow(A.prcheight - B.prcheight, 2) + Math.pow(A.prcweight - B.prcweight, 2) + Math.pow(A.prcfootsize - B.prcfootsize, 2));
}
function manhattan_dist(A, B){    
    return Math.abs(A.x - B.x) + Math.abs(A.y - B.y);
}
function classifyPerson(person){
    var i;    
    //skaitītāji
    var maleCount       = 0;
    var femaleCount     = 0;
    //variance (dispersija)
    var maleHeightVar   = 0;    //auguma dispersija
    var maleWeightVar   = 0;    //svara dispersija
    var maleFootSizeVar = 0;    //kājas izmērs dispersija
    var femaleHeightVar = 0; 
    var femaleWeightVar = 0; 
    var femaleFootSizeVar   = 0;
    //summārās vērtības
    var maleHeightSum   = 0;
    var maleWeightSum   = 0; 
    var maleFootSizeSum = 0;
    var femaleHeightSum = 0; 
    var femaleWeightSum = 0; 
    var femaleFootSizeSum   = 0;
    //vidējās vērtības
    var maleHeightAvg   = 0;
    var maleWeightAvg   = 0; 
    var maleFootSizeAvg = 0;
    var femaleHeightAvg = 0; 
    var femaleWeightAvg = 0; 
    var femaleFootSizeAvg   = 0;
    //varbūtības
    var maleHeightProb   = 0;
    var maleWeightProb   = 0; 
    var maleFootSizeProb = 0;
    var femaleHeightProb = 0; 
    var femaleWeightProb = 0; 
    var femaleFootSizeProb  = 0;
    var maleNaiveBayes   = 0;
    var femaleNaiveBayes = 0;
    //maksimālās vērtības
    var heightMax       = 0;
    var weightMax       = 0; 
    var footSizeMax     = 0;
    
    var nnPerson        = {};
    var nnPersonDst     = 0; //attālums līdz tuvākajam kaimiņam
    
    for(i=0; i< personBag.length; i++){
        
        if(personBag[i].gender == "vīrietis"){            
            maleCount++;                        
            maleHeightSum       += personBag[i].height;
            maleWeightSum       += personBag[i].weight;
            maleFootSizeSum     += personBag[i].footsize;            
        }
        if(personBag[i].gender == "sieviete"){
            femaleCount++;
            femaleHeightSum       += personBag[i].height;
            femaleWeightSum       += personBag[i].weight;
            femaleFootSizeSum     += personBag[i].footsize;
        }
        if(heightMax    < personBag[i].height){
            heightMax   = personBag[i].height;
        }
        if(weightMax    < personBag[i].weight){
            weightMax   = personBag[i].weight;
        }
        if(footSizeMax  < personBag[i].footsize){
            footSizeMax = personBag[i].footsize;
        }
    }
    maleHeightAvg       = maleHeightSum/maleCount;
    maleWeightAvg       = maleWeightSum/maleCount;
    maleFootSizeAvg     = maleFootSizeSum/maleCount;
    femaleHeightAvg     = femaleHeightSum/femaleCount;
    femaleWeightAvg     = femaleWeightSum/femaleCount;
    femaleFootSizeAvg   = femaleFootSizeSum/femaleCount;
    for(i=0; i< personBag.length; i++){
        if(personBag[i].gender == "vīrietis"){
            maleHeightVar       = maleHeightVar     + Math.pow(personBag[i].height      - maleHeightAvg, 2);            
            maleWeightVar       = maleWeightVar     + Math.pow(personBag[i].weight      - maleWeightAvg, 2);
            maleFootSizeVar     = maleFootSizeVar   + Math.pow(personBag[i].footsize    - maleFootSizeAvg, 2);
        }
        if(personBag[i].gender == "sieviete"){
            femaleHeightVar     = femaleHeightVar   + Math.pow(personBag[i].height      - femaleHeightAvg, 2);
            femaleWeightVar     = femaleWeightVar   + Math.pow(personBag[i].weight      - femaleWeightAvg, 2);
            femaleFootSizeVar   = femaleFootSizeVar + Math.pow(personBag[i].footsize    - femaleFootSizeAvg, 2);            
        }
        if(heightMax && weightMax && footSizeMax){
            personBag[i].prcheight      = personBag[i].height/heightMax;
            personBag[i].prcweight      = personBag[i].weight/weightMax;
            personBag[i].prcfootsize    = personBag[i].footsize/footSizeMax;
        }
    }
    if(heightMax && weightMax && footSizeMax){
        person.prcheight    = person.height/heightMax;
        person.prcweight    = person.weight/weightMax;
        person.prcfootsize  = person.footsize/footSizeMax;
    }
    //Naivā Beijesa aprēķināšana
    maleHeightVar       = maleHeightVar/maleCount;
    maleWeightVar       = maleWeightVar/maleCount;
    maleFootSizeVar     = maleFootSizeVar/maleCount;
    femaleHeightVar     = femaleHeightVar/femaleCount;
    femaleWeightVar     = femaleWeightVar/femaleCount;
    femaleFootSizeVar   = femaleFootSizeVar/femaleCount;
    
    maleHeightProb      = (1/Math.sqrt(2*Math.PI*maleHeightVar))*Math.exp(-Math.pow(person.height-maleHeightAvg,2)/(2*maleHeightVar));
    maleWeightProb      = (1/Math.sqrt(2*Math.PI*maleWeightVar))*Math.exp(-Math.pow(person.weight-maleWeightAvg,2)/(2*maleWeightVar));
    maleFootSizeProb    = (1/Math.sqrt(2*Math.PI*maleFootSizeVar))*Math.exp(-Math.pow(person.footsize-maleFootSizeAvg,2)/(2*maleFootSizeVar));
    maleNaiveBayes      = maleHeightProb * maleWeightProb * maleFootSizeProb * 0.5; // 0.5 ir varbūtība būt vīrietim vai sievietei
    
    femaleHeightProb    = (1/Math.sqrt(2*Math.PI*femaleHeightVar))*Math.exp(-Math.pow(person.height-femaleHeightAvg,2)/(2*femaleHeightVar));
    femaleWeightProb    = (1/Math.sqrt(2*Math.PI*femaleWeightVar))*Math.exp(-Math.pow(person.weight-femaleWeightAvg,2)/(2*femaleWeightVar));
    femaleFootSizeProb  = (1/Math.sqrt(2*Math.PI*femaleFootSizeVar))*Math.exp(-Math.pow(person.footsize-femaleFootSizeAvg,2)/(2*femaleFootSizeVar));
    femaleNaiveBayes    = femaleHeightProb * femaleWeightProb * femaleFootSizeProb * 0.5;
    
    resultBag  = [{
        gender: "vīrietis",
        heightProb: parseFloat(maleHeightProb).toFixed(6),
        weightProb: parseFloat(maleWeightProb).toFixed(6),
        footSizeProb: parseFloat(maleNaiveBayes).toFixed(6),
        naiveBayes: parseFloat(maleNaiveBayes).toFixed(6),
        class   : (maleNaiveBayes>femaleNaiveBayes)?"success":""
    },
    {
        gender: "sieviete",
        heightProb: parseFloat(femaleHeightProb).toFixed(6),
        weightProb: parseFloat(femaleWeightProb).toFixed(6),
        footSizeProb: parseFloat(femaleNaiveBayes).toFixed(6),
        naiveBayes: parseFloat(femaleNaiveBayes).toFixed(6),
        class   : (maleNaiveBayes<femaleNaiveBayes)?"success":""
    }
    ];
    buildBayesTable(resultBag);
    //Tuvākā kaimiņa atrašana
    for(i=0; i< personBag.length; i++){
        if(nnPersonDst == 0 || nnPersonDst > euclidean_dist_three_dimensional(person, personBag[i])){
            nnPerson = personBag[i];
            nnPersonDst = euclidean_dist_three_dimensional(person, personBag[i]);
        }        
    }
    buildNNTable(nnPerson);
    
}

function switchStages(switch_to_stage){
	stage = switch_to_stage;
	clear_stage();
	switch (stage)
	{		
		case 1:			
			$("#dist_stage").addClass("btn-primary");
			$("#nn-stage").removeClass("btn-primary");
                        $("#naive-stage").removeClass("btn-primary");
			$("#subtitle").text("Attāluma noteikšana");
                        $("#graphic-stage").show();
                        $("#table-stage").hide();
			$("#result_monitor").show();
		break;
		case 2:		
			$("#dist_stage").removeClass("btn-primary");
			$("#nn-stage").addClass("btn-primary");
                        $("#naive-stage").removeClass("btn-primary");
			$("#subtitle").text("Tuvākie kaimiņi");
                        $("#graphic-stage").show();
                        $("#table-stage").hide();
			$("#result_monitor").hide();		
		break;
                case 3:		
			$("#dist_stage").removeClass("btn-primary");
			$("#nn-stage").removeClass("btn-primary");
                        $("#naive-stage").addClass("btn-primary");                       
			$("#subtitle").text("Naivais Beijes vs tuvākais kaimiņš");
			$("#graphic-stage").hide();
                        $("#table-stage").show();
		break;
	}	
}
function generateSamplePoints(){
    var x_coords;
    var y_coords;
    var x_math_coords;
    var y_math_coords
    var i;
    var team;
    for(i=1; i <=3; i++)
    {
        x_math_coords = Math.floor((Math.random() * 460) + 1);
        y_math_coords = Math.floor((Math.random() * 460) + 1);
        x_coords = x_math_coords+20;
        y_coords = 480 - y_math_coords;
        point_name  = "P"+point_cnt;
        point_cnt++;
        switch(i)
        {
            case 1: team = "red";
            break;
            case 2: team = "green";
            break;
            case 3: team = "blue";
            break;
        }
        point = {
                name	: point_name,
                x   	: x_math_coords,
                y   	: y_math_coords,
                x_html	: x_coords,
                y_html	: y_coords,
                team	: team            
        };
        pointBag.push(point);
        ctx.beginPath();
        ctx.strokeStyle = point.team;
        ctx.lineWidth   = 3;
        ctx.arc(point.x_html,point.y_html,4,0,2*Math.PI);
        ctx.stroke();
        ctx.font = "12px Arial";
        ctx.fillText(point.name,x_coords-7,y_coords+20);        
    }
    buildPointTable();
}
function buildPointTable() {
    var i;
    $("#pointTable").empty();    
    
    $("#pointTable").append($('<thead>')
        .append($('<tr>')
            .append($('<th>')
                .text('Punkts')
            )
            .append($('<th>')
                .text('X')
            )
            .append($('<th>')
                .text('Y')
            )
            .append($('<th>')
                .text('Komanda')
            )
            .append($('<th>')
                .text('NN')
            )
        )
    );
    $("#pointTable").append($('<tbody>'));
    var tmpPoint        = {};
    var nn_point_name   = '';
    for (i = 0 ; i < pointBag.length ; i++) {
        tmpPoint        = pointBag[i];
        if(tmpPoint.hasOwnProperty('nn_point') && tmpPoint.nn_point.hasOwnProperty('name')){
            nn_point_name = tmpPoint.nn_point.name;
        }else{
            nn_point_name = '';
        }
        
        $("#pointTable").find('tbody')
        .append($('<tr>')
            .append($('<td>')
                .text(pointBag[i].name)
            )
            .append($('<td>')
                .text(pointBag[i].x)
            )
            .append($('<td>')
                .text(tmpPoint.y)
            )
            .append($('<td>')
                .text(pointBag[i].team)
            )
            .append($('<td>')
                .text(nn_point_name)
            )
        );
    }     
 }
function buildPersonTable() {
    var i;
    $("#personTable").empty();        
    $("#personTable").append($('<thead>')
        .append($('<tr>')
            .append($('<th>')
                .text('Dzimums')
            )
            .append($('<th>')
                .text('Augums(cm)')
            )
            .append($('<th>')
                .text('Svars(kg)')
            )
            .append($('<th>')
                .text('Kājas izmērs (cm)')
            )            
        )
    );
    $("#personTable").append($('<tbody>'));
    var tmpPoint        = {};
    var nn_point_name   = '';
    for (i = 0 ; i < personBag.length ; i++) {
        $("#personTable").find('tbody')
        .append($('<tr>')
            .append($('<td>')
                .text(personBag[i].gender)
            )
            .append($('<td>')
                .text(personBag[i].height)
            )
            .append($('<td>')
                .text(personBag[i].weight)
            )
            .append($('<td>')
                .text(personBag[i].footsize)
            )            
        );
    }     
 }
function buildBayesTable(resultBag) {
    var i;
    $("#bayesTable").empty();        
    $("#bayesTable").append($('<thead>')
        .append($('<tr>')
            .append($('<th>')
                .text('Dzimums')
            )
            .append($('<th>')
                .text('P(augums|dzimums)')
            )
            .append($('<th>')
                .text('P(svars|dzimums)')
            )
            .append($('<th>')
                .text('P(kājas izmērs|dzimums)')
            )
            .append($('<th>')
                .text('Naivais Beijes')
            )            
        )
    );
    $("#bayesTable").append($('<tbody>'));    
    for (i = 0 ; i < resultBag.length ; i++) {
        $("#bayesTable").find('tbody')
        .append($('<tr>').addClass(resultBag[i].class)
            .append($('<td>')
                .text(resultBag[i].gender)
            )
            .append($('<td>')
                .text(resultBag[i].heightProb)
            )
            .append($('<td>')
                .text(resultBag[i].weightProb)
            )
            .append($('<td>')
                .text(resultBag[i].footSizeProb)
            )            
            .append($('<td>')
                .text(resultBag[i].naiveBayes)
            )             
        );
    }     
 }
 function buildNNTable(nnPerson) {
    $("#nnTable").empty();        
    $("#nnTable").append($('<thead>')
        .append($('<tr>')
            .append($('<th>')
                .text('Dzimums')
            )
            .append($('<th>')
                .text('Augums')
            )
            .append($('<th>')
                .text('Svars')
            )
            .append($('<th>')
                .text('Kājas izmērs')
            )          
        )
    );
    $("#nnTable").append($('<tbody>')); 
    $("#nnTable").find('tbody')
    .append($('<tr>').addClass("success")
        .append($('<td>')
            .text(nnPerson.gender)
        )
        .append($('<td>')
            .text(nnPerson.height)
        )
        .append($('<td>')
            .text(nnPerson.weight)
        )
        .append($('<td>')
            .text(nnPerson.footsize)
        )          
    );  
 }
